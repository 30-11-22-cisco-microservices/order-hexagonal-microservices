package com.classpathio.order.config;

import org.springframework.boot.availability.ApplicationAvailability;
import org.springframework.boot.availability.LivenessState;
import org.springframework.boot.availability.ReadinessState;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/state")
@RequiredArgsConstructor
public class HealthRestController {
	
	private final ApplicationAvailability availability;
	private final ApplicationEventPublisher eventPublisher;
	 
	
	@PostMapping("/liveness")
	public String updateLivenessProbe() {
		LivenessState livenessState = this.availability.getLivenessState();
		LivenessState updatedLivenessState = livenessState == LivenessState.CORRECT ? LivenessState.BROKEN: LivenessState.CORRECT;
		eventPublisher.publishEvent(updatedLivenessState);
		return "LIVENESS";
		
	}
	
	@PostMapping("/readiness")
	public String updateReadinessProbe() {
		ReadinessState readinessState = this.availability.getReadinessState();
		ReadinessState updatedReadinessState = readinessState == ReadinessState.ACCEPTING_TRAFFIC ? ReadinessState.REFUSING_TRAFFIC : ReadinessState.ACCEPTING_TRAFFIC;
		eventPublisher.publishEvent(updatedReadinessState);
		return "READINESS";
		
	}

}
