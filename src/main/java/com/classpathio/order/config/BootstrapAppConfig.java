package com.classpathio.order.config;

import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

import com.classpathio.order.dao.OrderJpaRepository;
import com.classpathio.order.model.LineItem;
import com.classpathio.order.model.Order;
import com.github.javafaker.Faker;

import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class BootstrapAppConfig {

	private final OrderJpaRepository orderRepository;
	private final Faker faker = new Faker();

	@EventListener(ApplicationReadyEvent.class)
	public void onApplicationReadyEvent(ApplicationReadyEvent event) {
		String firstName = faker.name().firstName();
		IntStream.range(0, 100).forEach(index -> {
			Order order = Order.builder().name(firstName)
					.date(faker.date().past(4, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
					.build();
			IntStream.range(0, 3).forEach(value -> {
				LineItem lineItem = LineItem
										.builder()
											.name(faker.commerce().productName())
											.qty(faker.number().numberBetween(2, 5))
											.price(faker.number().randomDouble(2, 400, 600))
											.build();
				order.addLineItem(lineItem);
			});
			
			double totalOrderValue= order
										.getLineItems()
										.stream()
										.map(lineItem -> lineItem.getQty() * lineItem.getPrice())
										.reduce(Double::sum).get();
			order.setPrice(totalOrderValue);
			this.orderRepository.save(order);
		});

	}

}
