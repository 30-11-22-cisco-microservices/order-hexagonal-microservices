package com.classpathio.order.service;

import java.util.Set;

import com.classpathio.order.model.Order;

public interface OrderService {
	
	Order saveOrder(Order order);
	
	Set<Order> fetchAllOrders();
	
	Order findOrderById(long orderId);
	
	Order updateOrder(long orderId, Order order);
	
	void deleteOrderById(long orderId);
}
