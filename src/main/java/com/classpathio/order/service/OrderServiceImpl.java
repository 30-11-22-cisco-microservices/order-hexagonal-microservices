package com.classpathio.order.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.classpathio.order.dao.OrderJpaRepository;
import com.classpathio.order.model.Order;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {
	
	private final OrderJpaRepository orderRepository;

	@Override
	public Order saveOrder(Order order) {
		return this.orderRepository.save(order);
	}

	@Override
	public Set<Order> fetchAllOrders() {
		//return Set.copyOf(this.orderRepository.findAll());
		return new HashSet<>(this.orderRepository.findAll());
	}

	@Override
	public Order findOrderById(long orderId) {
		return this.orderRepository.findById(orderId).orElseThrow(() -> new IllegalArgumentException("invalid order id passed"));
	}

	@Override
	public Order updateOrder(long orderId, Order order) {
		Order existingOrder = this.findOrderById(orderId);
		existingOrder.setDate(order.getDate());
		existingOrder.setLineItems(order.getLineItems());
		existingOrder.setName(order.getName());
		existingOrder.setPrice(order.getPrice());
		return this.orderRepository.save(existingOrder);
	}

	@Override
	public void deleteOrderById(long orderId) {
		this.orderRepository.deleteById(orderId);
		
	}

}
